var http = require("http");
var https = require("https");
var url = require("url");

var fs = require('fs') ;
var ini = require('ini');
var querystring = require('querystring');
var parseIsoDuration = require('parse-iso-duration');
var timespan = require('timespan');

var readlineSync = require('readline-sync');

//http://stackoverflow.com/questions/736513/how-do-i-parse-a-url-into-hostname-and-path-in-javascript
var pathAnalysis = function (href) {
    var l = url.parse(href);
    return {
        secure: l.protocol == "https:",
        hostname: l.hostname,
        path: l.pathname+ (l.search!=null?l.search:""),
    };
};

var toIsoDuration = function(timespan1)
{
    var output = "P";
    if(timespan1.days != 0)
        output+=timespan1.days+"D";
    output+="T";
    if(timespan1.hours != 0)
        output+=timespan1.hours+"H";
    if(timespan1.minutes != 0)
        output+=timespan1.minutes+"M";
    if(output == "PT")
        output+="0S";
    return output;
}

var h = function (url, method, header, postdata, callback) {
    var pa = pathAnalysis(url);
    var options = {
        hostname: pa.hostname,
        path: pa.path,
        method: method,
        headers: header
    };


    var req = (pa.secure ? https : http).request(options, function (res) {
        res.setEncoding('utf8');
        var data = "";
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            console.log("http result: "+data);
            callback(JSON.parse(data));
        });
    });

    req.on('error', function (e) {
        console.log("Got error: "+e.message);
    });

    if (method == "POST"  ) {
        req.write(querystring.stringify(postdata));
    }
    if (method == "PATCH"  ) {
        req.write(JSON.stringify(postdata));
    }

    req.end();
    console.log(JSON.stringify(options) + "secure: " + pa.secure+" "+querystring.stringify(postdata))
}




if(!fs.existsSync("./config.ini"))
{
    var email1 = readlineSync.question('Trackingtime Email :');
    var password1 = readlineSync.question('Trackingtime password :');
    var ttproject1 = readlineSync.question('Trackingtime project name :');
    var apikey1 = readlineSync.question('Openproject api key :');
    fs.writeFileSync('./config.ini', ini.stringify( { "email":email1, "password":password1,"apikey":apikey1, "ttproject":ttproject1 }));
}
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'))

 var TrackingtimeAuth = 'Basic ' + new Buffer(config.email + ':' + config.password).toString('base64');
 var OpenProjectAuth = 'Basic ' + new Buffer('apikey:' + config.apikey).toString('base64');
console.log("Getting trackingtime tasks");
 h('https://app.trackingtime.co/api/v2/tasks?filter=ALL',"GET",{'Authorization': TrackingtimeAuth },null,function(TrackingtimeData){
     console.log("Getting messager tasks");
     h('http://messager.dinf.usherbrooke.ca/api/v3/projects/verrou-2/work_packages?pageSize=9999', "GET", {'Authorization': OpenProjectAuth }, null, function (messagerData) {
         messagerData._embedded.elements.forEach(function (messagerTask) {
                 var isroot = messagerTask._links.children && (messagerTask._links.children.length > 0)
                var found = false;
                var prefix = "OP"+messagerTask.id+" - ";
                var taskname = prefix+messagerTask.subject;
                opEstimated = null;
                ttDone = null;
                calcRemaining = null;
                var ttassignee = 0;
                var ttTask = null;
             var opClosed = (messagerTask._links.status.title=="Closed");
             var opProgress = (messagerTask._links.status.title=="In progress");

                if(messagerTask._links.assignee.href)
                {
                    var tmp24=messagerTask._links.assignee.href;
                    var opassignee = tmp24.substr(tmp24.lastIndexOf("/")+1);
                    if(config["op"+opassignee])
                    {
                        ttassignee = config["op"+opassignee].substr(2);
                    }
                }

                if(messagerTask.estimatedTime)
                {
                    opEstimated = new timespan.TimeSpan(0/*ms*/,0/*seconds*/
                        ,Math.floor((parseIsoDuration(messagerTask.estimatedTime)/1000/60)%60)/*minute*/
                        ,Math.floor((parseIsoDuration(messagerTask.estimatedTime)/1000/60)/60)/*hour*/
                    );
                }

                TrackingtimeData.data.forEach(function (ttItem) {
                    if(ttItem.project == config.ttproject && ttItem.name.indexOf(prefix)!=-1)
                    {
                        if(found)
                        {
                            console.log("FOUND MULTIPLE TIME: OP"+messagerTask.id)
                        }
                        else
                        {
                            found = true;
                            ttDone = new timespan.TimeSpan(0/*ms*/,0/*seconds*/
                                ,Math.floor((ttItem.accumulated_time/60)%60)/*minute*/
                                ,Math.floor((ttItem.accumulated_time/60)/60)/*hour*/
                            );
                            ttTask = ttItem;
                        }
                    }
                });

                if(opEstimated != null && ttDone!=null)
                {
                    calcRemaining = timespan.clone(opEstimated);
                    calcRemaining.subtract(ttDone);
                    if(calcRemaining.totalMilliseconds()<0)
                    {
                        calcRemaining = new timespan.TimeSpan(0,0,0);
                        console.log("WARNING: Remaining is now under zero!!");
                    }
                    console.log((isroot ? "BIG" : "SMALL") + " - OP"+messagerTask.id+" - " + messagerTask.subject + " "+ttDone.hours+":"+ttDone.minutes+"/"+opEstimated.hours+":"+opEstimated.minutes+" Remaining "+calcRemaining.hours+":"+calcRemaining.minutes);
                }
             else
                    console.log((isroot ? "BIG" : "SMALL") + " - OP"+messagerTask.id+" - " + messagerTask.subject + (opEstimated == null?" [NO ESTIMATION] ":"")+(ttDone == null?" [NO WORK DONE] ":""));




                if(!found && !isroot && opProgress)
                {
                    console.log("Should create task "+taskname)
                    if(true)
                    {
                        h('https://app.trackingtime.co/api/v2/tasks/add', "POST", {'Authorization': TrackingtimeAuth,'Content-Type': 'application/x-www-form-urlencoded' },
                            {
                                project_name:config.ttproject,
                                notify_by_email:"false",
                                name: taskname,
                                due_date: messagerTask.dueDate,
                                user_id: ttassignee,
                                estimated_time: opEstimated.totalHours()
                            }
                            , function (callback123) {
                            console.log("Created task "+taskname)
                        });
                    }
                    else
                    {
                        console.log("Task creation disabled "+taskname)
                    }
                }

                if(found  && !isroot)
                {
                    var updateOp={};
                    var hasToUpdate = false;


                    if(calcRemaining != null && toIsoDuration(calcRemaining)!=messagerTask.remainingTime)
                    {
                        console.log("I should update the OP/REMAINING from "+messagerTask.remainingTime+" to "+toIsoDuration(calcRemaining));
                        updateOp.remainingTime=toIsoDuration(calcRemaining);
                        hasToUpdate = true;
                    }

                    if(found && ttTask.is_archived && !opClosed)
                    {
                        console.log("I should update the OP/CLOSED from "+(opClosed?"Closed":"New")+" to "+(ttTask.is_archived?"Closed":"New"));
                        if(updateOp._links == null)
                            updateOp._links = {};
                        if(updateOp._links.status == null)
                            updateOp._links.status = {};
                        updateOp._links.status.title = "Closed";
                        updateOp._links.status.href = "/api/v3/statuses/13";
                        hasToUpdate = true;
                    }

                    if(hasToUpdate)
                    {
                        updateOp.lockVersion = messagerTask.lockVersion;
                        console.log("OP has to be modified: "+JSON.stringify(updateOp));
                        if(true)
                        {
                            h('http://messager.dinf.usherbrooke.ca/api/v3/work_packages/'+messagerTask.id, "PATCH", {'Authorization': OpenProjectAuth,'Content-Type': 'application/json' },
                                updateOp
                                , function (callback123) {
                                    console.log("Updated task "+taskname)
                                });
                        }
                        else
                        {
                            console.log("OP task updating disabled "+taskname)
                        }
                    }
                    else
                    {
                        //console.log("OP is ok as is.");
                    }

                }
             }
         );
     });
 });

